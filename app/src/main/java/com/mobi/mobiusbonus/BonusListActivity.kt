package com.mobi.mobiusbonus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*

class BonusListActivity : AppCompatActivity() {

    private lateinit var viewModel: BonusListViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(BonusListViewModel::class.java)

        progress_bar_loading.visibility = View.VISIBLE
        viewModel.getBonusList().observe(this, Observer {
            println(it.size)
            progress_bar_loading.visibility = View.GONE
        })
    }
}