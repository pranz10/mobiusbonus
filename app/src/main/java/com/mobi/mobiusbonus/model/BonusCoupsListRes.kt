package com.mobi.mobiusbonus.model

import com.google.gson.annotations.SerializedName

data class BonusCoupsListRes(
    @SerializedName("id")
    var id: String = "",

    @SerializedName("valid_from")
    var validFrom: String = "",

    @SerializedName("valid_until")
    var validUntil: String = "",

    @SerializedName("is_active")
    var isActive: Boolean = false,

    @SerializedName("is_deleted")
    var isDeleted: Boolean = false,

    @SerializedName("created_at")
    var createdAt: String = "",

    @SerializedName("last_updated_at")
    var lastUpdatedAt: String = "",

    @SerializedName("code")
    var code: String = "",

    @SerializedName("bonus_image_front")
    var bonusImageFront: String = "",

    @SerializedName("bonus_image_back")
    var bonusImageBack: String = "",

    @SerializedName("user_redeem_limit")
    var userRedeemLimit: Int = 0,

    @SerializedName("user_limit")
    var userLimit: Int = 0,

    @SerializedName("tab_type")
    var tabType: String = "",

    @SerializedName("ribbon_msg")
    var ribbonMsg: String = "",

    @SerializedName("is_bonus_booster_enabled")
    var isBonusBoosterEnabled: Boolean = false,

    @SerializedName("wager_bonus_expiry")
    var wagerBonusExpiry: Int = 0,

    @SerializedName("wager_to_release_ratio_numerator")
    var wagerToReleaseRatioNumerator: Int = 0,

    @SerializedName("wager_to_release_ratio_denominator")
    var wagerToReleaseRatioDenominator: Int = 0,

    @SerializedName("slabs")
    var slabs: MutableList<Slab> = mutableListOf(),

    @SerializedName("user_segmentation_type")
    var userSegmentationType: String = "",

    @SerializedName("eligibility_user_levels")
    var eligibilityUserLevels: MutableList<Int> = mutableListOf(),

    @SerializedName("eligibility_user_segments")
    var eligibilityUserSegments: MutableList<String> = mutableListOf(),

    @SerializedName("visibility_user_levels")
    var visibilityUserLevels: MutableList<Int> = mutableListOf(),

    @SerializedName("visibility_user_segments")
    var visibilityUserSegments: MutableList<String> = mutableListOf(),

    @SerializedName("days_since_last_purchase_min")
    var daysSinceLastPurchaseMin: Int = 0,

    @SerializedName("_cls")
    var cls: String = "",

    @SerializedName("campaign")
    var campaign: String = "",

    @SerializedName("bonus_booster")
    var bonusBooster: String = ""
)



data class Slab(

    @SerializedName("name")
    var name: String = "",

    @SerializedName("num")
    var num: Int = 0,

    @SerializedName("min")
    var min: Double = 0.0,

    @SerializedName("max")
    var max: Double = 0.0,

    @SerializedName("wagered_percent")
    var wageredPercent: Double = 0.0,

    @SerializedName("wagered_max")
    var wageredMax: Double = 0.0,

    @SerializedName("otc_percent")
    var otcPercent: Double = 0.0,

    @SerializedName("otc_max")
    var otcMax: Double = 0.0
)