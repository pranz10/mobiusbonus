package com.mobi.mobiusbonus

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mobi.mobiusbonus.model.BonusCoupsListRes
import com.mobi.mobiusbonus.repo.BonusListRepo

class BonusListViewModel: ViewModel() {

    private lateinit var bonusList : MutableLiveData<MutableList<BonusCoupsListRes>>

    fun getBonusList(): LiveData<MutableList<BonusCoupsListRes>> {
        bonusList = BonusListRepo.getBonusCouponsList()
        return bonusList
    }
}