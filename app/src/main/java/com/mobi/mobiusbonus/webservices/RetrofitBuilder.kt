package com.mobi.mobiusbonus.webservices

import com.google.gson.GsonBuilder
import com.mobi.mobiusbonus.BuildConfig

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 *  Retrofit build class
 */
object RetrofitBuilder {

    private const val BASE_URL:String = "https://run.mocky.io/v3/"

    /**
     * The build method
     *
     * @return The Retrofit build
     */
    fun build(): Retrofit {
        val httpClient = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
        }

        httpClient.addInterceptor { chain ->
            val original = chain.request()

            // Request customization: add request headers
            val requestBuilder = original.newBuilder()
                .header("Content-Type", "application/json")
                .url(original.url())

            val request = requestBuilder.build()
            chain.proceed(request)
        }

        val client = httpClient.build()

        val gson = GsonBuilder().setLenient().create()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()

    }
}
