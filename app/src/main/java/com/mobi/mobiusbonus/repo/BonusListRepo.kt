package com.mobi.mobiusbonus.repo

import androidx.lifecycle.MutableLiveData
import com.mobi.mobiusbonus.model.BonusCoupsListRes
import com.mobi.mobiusbonus.webservices.RetrofitBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object BonusListRepo {

    private val bonusCouponsRequests : BonusCouponsRequests by lazy {
        RetrofitBuilder.build().create(BonusCouponsRequests::class.java)
    }
    private val bonusCouponsList = MutableLiveData<MutableList<BonusCoupsListRes>>()

    fun getBonusCouponsList(): MutableLiveData<MutableList<BonusCoupsListRes>> {
        val call = bonusCouponsRequests.getBonusCouponsList()
        call.enqueue(object : Callback<MutableList<BonusCoupsListRes>>{
            override fun onFailure(call: Call<MutableList<BonusCoupsListRes>>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<MutableList<BonusCoupsListRes>>,
                response: Response<MutableList<BonusCoupsListRes>>) {
                val data = response.body()
                if(data != null) {
                    bonusCouponsList.value = data!!
                }
            }
        })
        return bonusCouponsList
    }
}