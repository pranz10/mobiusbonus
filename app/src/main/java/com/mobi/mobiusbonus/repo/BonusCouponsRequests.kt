package com.mobi.mobiusbonus.repo


import com.mobi.mobiusbonus.model.BonusCoupsListRes
import retrofit2.Call
import retrofit2.http.GET

interface BonusCouponsRequests {

    @GET("4c663239-03af-49b5-bcb3-0b0c41565bd2")
    fun getBonusCouponsList(): Call<MutableList<BonusCoupsListRes>>
}